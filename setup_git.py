#!/usr/bin/env python

from setuptools import setup

setup(name='gldict',
      version='1.0.0',
      description='Extension of the dictionary object',
      author='Horatio Voicu',
      author_email='horatio.fm@gmail.com',
      url='https://bitbucket.org/horatio-fm/gldict',
      py_modules=['gldict'],
      # packages=['gldict'],
      )
