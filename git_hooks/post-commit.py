"""
in .\gldict\.git\hooks\post-commit
#!/bin/sh
# important that it's got the .exe on the end!
unset GIT_DIR
cmd.exe /c "python.exe git_hooks/post-commit.py"
"""

import os
import subprocess

repository_directory = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


def resolve_path(executable):
    if os.path.sep in executable:
        raise ValueError("Invalid filename: %s" % executable)

    path = os.environ.get("PATH", "").split(os.pathsep)
    # PATHEXT tells us which extensions an executable may have
    path_exts = os.environ.get("PATHEXT", ".exe;.bat;.cmd").split(";")
    has_ext = os.path.splitext(executable)[1] in path_exts
    if not has_ext:
        exts = path_exts
    else:
        # Don't try to append any extensions
        exts = [""]

    for d in path:
        try:
            for ext in exts:
                exepath = os.path.join(d, executable + ext)
                if os.access(exepath, os.X_OK):
                    return exepath
        except OSError:
            pass

    return None


git = resolve_path("git")

proc = subprocess.Popen('"{0}" describe --tags'.format(git), stdout=subprocess.PIPE, shell=True)
version = proc.communicate()[0].decode("utf-8").rstrip()


proc = subprocess.Popen('"{0}" rev-parse --abbrev-ref HEAD'.format(git), stdout=subprocess.PIPE, shell=True)
current_branch = proc.communicate()[0].decode("utf-8").rstrip()

executable_filenames = [
    os.path.join(repository_directory, "gldict.py"),
    os.path.join(repository_directory, "setup.py"),
    ]
filename_from_repos = [
    os.path.join(repository_directory, "gldict_git.py"),
    os.path.join(repository_directory, "setup_git.py"),
    ]

proc = subprocess.Popen('"{0}" show -s --format=%ci HEAD'.format(git), stdout=subprocess.PIPE, shell=True)
time_of_commit = proc.communicate()[0].decode("utf-8").rstrip()

executable_version = "__".join([version, time_of_commit.replace(" ", "_"), current_branch])

for filename_from_repo, executable_filename in zip(filename_from_repos, executable_filenames):
    with open(filename_from_repo, "r") as f_in:
        with open(executable_filename, "w") as f_out:
            for line in f_in:
                if "__version__ = 'v1.0.0'" in line:
                    msg = "__version__ = '{}'\n".format(executable_version)
                    f_out.write(msg)
                elif "version='1.0.0'," in line:
                    msg = line.replace("version='1.0.0',", "version='{}',".format(executable_version))
                    f_out.write(msg)
                else:
                    f_out.write(line)