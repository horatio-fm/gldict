
import os
import json
import collections

GlDict_keyword = "GlDict_mitJoo7sw9poi2Yy"

__version__ = 'v1.0.0'


class GlDict(dict):

    directory = None

    def __init__(self, name):
        super().__init__()
        self.name = name
        if not GlDict.directory:
            GlDict.directory = "." + name
            if not os.path.exists(GlDict.directory):
                os.mkdir(GlDict.directory)
        self.file_location = os.path.join(GlDict.directory, self.name + '.json')

    def __getattr__(self, name):
        if name in self:
            return self[name]
        else:
            raise AttributeError("No such attribute: " + name)

    def __setattr__(self, name, value):
        self[name] = value

    def __delattr__(self, name):
        if name in self:
            del self[name]
        else:
            raise AttributeError("No such attribute: " + name)

    def save(self):
        current_level_dict = {}
        for k in self:
            if isinstance(self[k], GlDict):
                self[k].save()
                current_level_dict[k] = GlDict_keyword
            else:
                current_level_dict[k] = self[k]

        with open(self.file_location, 'w') as fp:
            json.dump(current_level_dict, fp)

    def save_ordered(self):
        current_level_dict = {}
        for k in self:
            if isinstance(self[k], GlDict):
                self[k].save_ordered()
                current_level_dict[k] = GlDict_keyword
            else:
                current_level_dict[k] = self[k]

        current_level_dict = collections.OrderedDict(sorted(current_level_dict.items()))
        with open(self.file_location, 'w') as fp:
            json.dump(current_level_dict, fp)

    def load(self):
        if not os.path.exists(self.file_location):
            self.save()

        with open(self.file_location, 'r') as fp:
            temp = json.load(fp)
            for ele in temp:
                if ele == "file_location":
                    if self.file_location != temp[ele]:
                        raise ValueError("GlDict file_location: {} != {}".format(self.file_location, temp[ele]))
                    continue
                if ele == "name":
                    if self.name != temp[ele]:
                        raise ValueError("GlDict file_location: {} != {}".format(self.name, temp[ele]))
                    continue
                if temp[ele] == GlDict_keyword:
                    self[ele] = GlDict(self.name + "." + ele)
                    self[ele].load()
                    continue
                self[ele] = temp[ele]

